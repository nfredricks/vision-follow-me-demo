#Turtlebot Follow Me Demo
This demo allows a Turtlebot to track and follow an April Tag in realtime.

##ROS Dependancies
* april_tk
* rosjava_core
* usb_cam

##Installation (Fuerte Only)
NOTE: If the above dependancies are already in your ROS path you may skip the rest of this section and execute `source minimal.sh`

The repository contains a install.sh script that will setup a new ROS overlay, build the demo and take care of the downloading and installation of dependancy repositories listed above. Execute the script with `source install.sh`

##Camera Customization
A predefined launch file has been included for use with the usb_cam node. It is named usb_cam.launch and is located in the april_follow_me/launch directory. It is recommended that you first use OpenCV or similar software to determine the intrinsics of the camera you plan use with the demo. Once that has been accomplished, edit the usb_cam launch file with the determined x and y focal lengths.

##Usage
Once the installation and customization are complete you can invoke the demo with:

`roslaunch april_follow_me follow_me.launch`

This will bringup the following:

* turtlbot_node
* usb_cam
* april_follow_me

To visualize the robot and april tag detection use:

``rosrun rviz rviz -d `rospack find april_follow_me`/config/follow.vcg``

##Parameters
The following parameters are customizable through the dynamic reconfigure GUI or the follow_me.launch file:

* Angular proportional gain (~ang_P)
* Linear proportional gain (~lin_P)
* Observation noise covariance (~obs_cov)
* Process noise covariance (~process_cov)
* Goal buffer distance (~goal_buffer)
* April Tag Family to track (~tag_family)

To use the dynamic reconfigure GUI use:

`rosrun dynamic_reconfigure reconfigure_gui`

##Video
Follow this link to see the demo in action! <http://www.youtube.com/watch?v=6qbPpwqaBto>




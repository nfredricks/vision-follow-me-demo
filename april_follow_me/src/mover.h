
#include <ros/ros.h>
#include <iostream>
#include <fstream>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Transform.h>
#include <visualization_msgs/Marker.h>
#include <april_msgs/TagPoseArray.h>
#include <signal.h>
#include <termios.h>
#include <stdio.h>
#include <Eigen/Dense>
#include <dynamic_reconfigure/server.h>
#include <april_follow_me/parametersConfig.h>

using namespace std;

namespace Eigen{
    typedef Eigen::Matrix< float, 18, 1> Vector18f; 
    typedef Eigen::Matrix< float, 18, 18> Matrix18f; 
    typedef Eigen::Matrix< float, 6, 1> Vector6f; 
    typedef Eigen::Matrix< float, 6, 6> Matrix6f; 
}


class KalmanFilter{
    public:
        KalmanFilter();
        void kalmanUpdate(Eigen::Vector6f);
        void kalmanUpdate();
        Eigen::Vector18f getState();
        void setCovariance(const double, const double);
    private:
        ros::NodeHandle n;
        
        //observation and process covariance
        Eigen::Matrix6f R;
        Eigen::Matrix18f Q;

        //kalman gain
        Eigen::Matrix<float, 18, 6> K;

        //Current and future estimate
        Eigen::Vector18f x, xHat;

        //Current and future estimation covariance
        Eigen::Matrix18f p, pHat;

        //sensor to state matrix
        Eigen::Matrix<float, 6, 18> h;
        Eigen::Matrix<float, 18, 6> hT;

        //state transition matrix
        Eigen::Matrix18f F;

        ros::Time last_update;
        ros::Time current_time;

        void estUpdate(const Eigen::Vector6f);
        void stateTransitionUpdate(const float);
        void gainUpdate();
        void predictUpdate();
};

class PID{
    public:
        PID();
        Eigen::Matrix<float, 2, 1> update(Eigen::Matrix<float, 2, 2>);
        void setGains(const double, const double, const double, const double, const double, const double);

    private:
        Eigen::Matrix<float, 2, 1> Kp, Ki, Kd;
        Eigen::Matrix<float, 2, 2> ePrev, eRunning;
        ros::Time current_time, last_update;

};


class TurtlebotFollowMe{
    public:
        TurtlebotFollowMe();
        void moveWatchDog();
        void tagWatchDog();
        void paramCallback(april_follow_me::parametersConfig&, uint32_t);
        inline PID& getPID(){return control;}
        inline KalmanFilter& getKF(){return filter;}
        inline void setBuffer(const double value){ goal_buffer = value;}
        inline void stop(){ publish(0.0,0.0);}
        inline void setTagFamily(const std::string family) {tag_family = family;}
        inline void setExplore(const bool value){ explore_set = value;}
    private:
        double lin, alg;
        double goal_buffer;
        std::string tag_family;
        void publish(double, double);
        void gotTag(const april_msgs::TagPoseArray);
        bool explore; 
        bool explore_set;
        ros::NodeHandle n;
        ros::Publisher mov_pub;
        ros::Publisher vis_pub;
        ros::Publisher tag_pub;
        ros::Time last_publish;
        ros::Time last_message;
        ros::Time last_tag;
        ros::Subscriber tag_sub;
        tf::TransformListener tf_listener;
        void driveToWaypoint();
        Eigen::Vector6f quatToEuler(const tf::StampedTransform);
        KalmanFilter filter;
        PID control;
};



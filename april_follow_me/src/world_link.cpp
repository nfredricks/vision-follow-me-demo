#include "world_link.h"


WorldLink::WorldLink() {
    path_pub = node.advertise<nav_msgs::Path>("bot_path", 1);
    path_sub = node.subscribe("/bot_path", 1, &WorldLink::gotPath, this);
    tag_path_pub = node.advertise<nav_msgs::Path>("tag_path", 1);
    tag_path_sub = node.subscribe("/tag_path", 1, &WorldLink::gotTagPath, this);
    pose_sub = node.subscribe("/robot_pose_ekf/odom", 1, &WorldLink::gotPose, this);
    tag_pose_sub = node.subscribe("/est_tag_pose", 1, &WorldLink::gotTagPose, this);
}

void WorldLink::gotPose(const geometry_msgs::PoseWithCovarianceStamped& msg){
    static tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z) );
    transform.setRotation( tf::Quaternion(msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "odom"));


    geometry_msgs::PoseStamped cur_pose = dropCovariance(msg);
    current_path.header.frame_id = "/world";
    current_path.header.stamp = msg.header.stamp;
    current_path.poses.push_back(cur_pose);
    path_pub.publish(current_path);


}

void WorldLink::gotTagPose(const geometry_msgs::PoseStamped& msg){


    geometry_msgs::PoseStamped cur_pose = msg;
    current_tag_path.header.frame_id = "/world";
    current_tag_path.header.stamp = msg.header.stamp;
    current_tag_path.poses.push_back(cur_pose);
    tag_path_pub.publish(current_tag_path);

}


/*
*covert msg to posestamped for path array
*/
geometry_msgs::PoseStamped WorldLink::dropCovariance(const geometry_msgs::PoseWithCovarianceStamped& msg){
    geometry_msgs::PoseStamped cur_pose;
    cur_pose.pose.position.x = msg.pose.pose.position.x;
    cur_pose.pose.position.y = msg.pose.pose.position.y;
    cur_pose.pose.position.z = msg.pose.pose.position.z;
    cur_pose.pose.orientation.x = msg.pose.pose.orientation.x;
    cur_pose.pose.orientation.y = msg.pose.pose.orientation.y;
    cur_pose.pose.orientation.z = msg.pose.pose.orientation.z;
    cur_pose.pose.orientation.w = msg.pose.pose.orientation.w;
    cur_pose.header.frame_id = msg.header.frame_id;
    cur_pose.header.stamp = msg.header.stamp;
    return cur_pose;
}


void WorldLink::gotPath(const nav_msgs::Path& msg){
    current_path = msg;
}
void WorldLink::gotTagPath(const nav_msgs::Path& msg){
    current_tag_path = msg;
}

int main(int argc, char** argv){
    ros::init(argc, argv, "turtlebot_tf_broadcaster");
    ros::NodeHandle node;
    WorldLink linker;


    ros::spin();
    return 0;
};

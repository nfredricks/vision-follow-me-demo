#include "mover.h"
ofstream robotFile ("/home/live/robot_pose.txt");
ofstream aprilFile ("/home/live/april_pose.txt");
ofstream tagFile ("/home/live/tag_pose.txt");

tf::StampedTransform robot_transform;
tf::StampedTransform april_transform;


TurtlebotFollowMe::TurtlebotFollowMe(){
    explore = true;
    last_tag = last_publish = last_message = ros::Time::now();
    mov_pub = n.advertise<geometry_msgs::Twist>("cmd_vel",1);
    tag_sub = n.subscribe<april_msgs::TagPoseArray>("tags",1,&TurtlebotFollowMe::gotTag, this);
    vis_pub = n.advertise<visualization_msgs::Marker>( "visualization_marker", 0 );
    tag_pub = n.advertise<geometry_msgs::PoseStamped>( "est_tag_pose", 0 );
}

KalmanFilter::KalmanFilter(){
    Eigen::Vector18f a;
    a.fill(0.1);
    p.setZero();
    p.diagonal() = a;

    //only xyz rph have correspondences
    h.fill(0.0);
    h(0,0) = h(1,1) = h(2,2) = h(3,9) = h(4,10) = h(5,11) = 1;
    hT = h.transpose();

    //create state transition matrix
    F.setIdentity(); 


}

PID::PID(){
    ePrev.setZero();
    eRunning.setZero();
    Ki << 0.0, 0.0;
    Kp << 0.5, 0.5;
    Kd << 0.0, 0.0;


}
TurtlebotFollowMe* Gfollower = 0;

void halt(int sig){

    /*
     *Make sure the robot stops!
     */
    if(Gfollower != 0){
        Gfollower->stop();
    }
    
    ros::shutdown();
    exit(0);
}

void TurtlebotFollowMe::paramCallback(april_follow_me::parametersConfig &config, uint32_t level) {

    getPID().setGains(config.lin_P, config.lin_I, config.lin_D, config.ang_P, config.ang_I, config.ang_D);
    getKF().setCovariance(config.process_cov, config.obs_cov);
    setBuffer(config.goal_buffer);
    setTagFamily(config.tag_family);
    setExplore(config.explore);

}

int main(int argc, char** argv){
    ros::init(argc, argv, "turtlebot_follow_me");
    
    ros::NodeHandle n;
    TurtlebotFollowMe follower; 
    Gfollower = &follower;

    dynamic_reconfigure::Server<april_follow_me::parametersConfig> server;
    dynamic_reconfigure::Server<april_follow_me::parametersConfig>::CallbackType f;

    f = boost::bind(&TurtlebotFollowMe::paramCallback, &follower, _1, _2);
    server.setCallback(f);


    signal(SIGINT,halt);

    ros::Timer moveTimer = n.createTimer(ros::Duration(0.1), boost::bind(&TurtlebotFollowMe::moveWatchDog, &follower));
    ros::Timer waypointTimer = n.createTimer(ros::Duration(0.1), boost::bind(&TurtlebotFollowMe::tagWatchDog, &follower));
    robotFile.setf(ios::scientific);
    aprilFile.setf(ios::scientific);
    tagFile.setf(ios::scientific);

    ros::spin();
    tagFile.close();
    robotFile.close();
    aprilFile.close();
    
    return(0);
}


//no commands stop robot
void TurtlebotFollowMe::moveWatchDog(){
    if(ros::Time::now() > last_publish + ros::Duration(1.0)){
        publish(0,0);
        ROS_INFO("stop robot");
      }
}
void TurtlebotFollowMe::tagWatchDog(){
    if(ros::Time::now() > last_tag + ros::Duration(2.0)){
        //we lost the tag for too long, refind it
        explore = (ros::Time::now() > last_tag + ros::Duration(1.0)) ? true : explore;
        if(explore){
            if(explore_set){
                ROS_INFO("Exploring.....................");
                //spin around until we find another tag
                publish(0.4,0);
            }
            else{
                ROS_INFO("Exploring disabled............");
                publish(0,0);
            }
        }
        //do kalman filter prediction and update the waypoint
        else{
            ROS_INFO("Lost Tag. Predicting trajectory...");
            filter.kalmanUpdate();
            driveToWaypoint();
        }
    }else{
        driveToWaypoint();
    }
}

void TurtlebotFollowMe::publish(double ang, double lin){
    last_publish = ros::Time::now();
    geometry_msgs::Twist vel;
    if(ang < 0 ){
        vel.angular.z = std::max(ang, -1.25);
    }
    else{
        vel.angular.z = std::min(ang, 1.25);
    }
    vel.linear.x = lin;
    mov_pub.publish(vel);
    return;
}

void TurtlebotFollowMe::gotTag(const april_msgs::TagPoseArray tag_pose){
    if(tag_pose.tags.size() > 0){
        last_tag = ros::Time::now();
        explore = false;
        ROS_INFO("Following...................");
        /*
         *lookup all transforms at the tag pose timestamp and store them for later use
         */
        try{
            tf_listener.waitForTransform("/world", tag_family + ".0", tag_pose.header.stamp, ros::Duration(1.0));
            tf_listener.lookupTransform("/world", tag_family + ".0", tag_pose.header.stamp, april_transform);

            try{
                tf_listener.waitForTransform("/world", "/base_link", tag_pose.header.stamp, ros::Duration(1.0));
                tf_listener.lookupTransform("/world", "/base_link", tag_pose.header.stamp, robot_transform);

                Eigen::Vector6f global_pose = quatToEuler(april_transform);

                filter.kalmanUpdate(global_pose);
            }
            catch (tf::TransformException ex){
                ROS_ERROR("%s",ex.what());
            }
        }
        catch (tf::TransformException ex){
            ROS_ERROR("%s",ex.what());
        }

    }

}
Eigen::Vector6f TurtlebotFollowMe::quatToEuler(const tf::StampedTransform transform){
    /*
     *Convert transform to xyzrph
     */
    Eigen::Vector6f global_pose;
    tf::Vector3 origin = transform.getOrigin();
    global_pose(0) = origin.x(); global_pose(1) = origin.y(); global_pose(2) = origin.z(); 

    tf::Quaternion quatrot = transform.getRotation();
    float q0, q1, q2, q3;
    q0 = (float)quatrot.x(); q1 = (float)quatrot.y();  q2 = (float)quatrot.z(); q3 = (float)quatrot.w();
    global_pose(3) = atan2(2.0*(q0*q1 + q2*q3), 1.0 - 2.0*(pow(q1,2)+pow(q2,2)));
    global_pose(4) = asin(2.0*(q0*q2 - q3*q1));
    global_pose(5) = atan2(2.0*(q0*q3 + q1*q2), 1.0 - 2.0*(pow(q2,2) + pow(q3,2)));
    return global_pose;
}

void TurtlebotFollowMe::driveToWaypoint(){
    Eigen::Vector18f X = filter.getState();

    /*
     *Publish the filtered tag pose
     */

    visualization_msgs::Marker marker;
    geometry_msgs::PoseStamped marker_pose;
    marker_pose.header.stamp = ros::Time::now();
    marker.header.frame_id = "/world";
    marker.header.stamp = ros::Time::now();
    marker.ns = "my_namespace";
    marker.id = 0;
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = X(0);
    marker_pose.pose.position.x = marker.pose.position.x;
    marker.pose.position.y = X(1);
    marker_pose.pose.position.y = marker.pose.position.y;
    marker.pose.position.z = X(2);
    marker_pose.pose.position.z = marker.pose.position.z;
    /*
     *convert rpy to quat
     */
    marker.pose.orientation.x = cos(X(9)/2)*cos(X(10)/2)*cos(X(11)/2) + sin(X(9)/2)*sin(X(10)/2)*sin(X(11)/2);
    marker_pose.pose.orientation.x = marker.pose.orientation.x;
    marker.pose.orientation.y = sin(X(9)/2)*cos(X(10)/2)*cos(X(11)/2) - cos(X(9)/2)*sin(X(10)/2)*sin(X(11)/2);
    marker_pose.pose.orientation.y = marker.pose.orientation.y;
    marker.pose.orientation.z = cos(X(9)/2)*sin(X(10)/2)*cos(X(11)/2) + sin(X(9)/2)*cos(X(10)/2)*sin(X(11)/2);
    marker_pose.pose.orientation.z = marker.pose.orientation.z;
    marker.pose.orientation.w = cos(X(9)/2)*cos(X(10)/2)*sin(X(11)/2) - sin(X(9)/2)*sin(X(10)/2)*cos(X(11)/2);
    marker_pose.pose.orientation.w = marker.pose.orientation.w;
    marker.scale.x = 0.2;
    marker.scale.y = 0.2;
    marker.scale.z = 0;
    marker.color.a = 1.0;
    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 0.0;
    vis_pub.publish( marker );
    tag_pub.publish( marker_pose );

    Eigen::Vector2f tag_xy;
    tag_xy << X(0), X(1);
    //ROS_INFO("xDist: %f",tag_xy(0));
    //ROS_INFO("yDist: %f",tag_xy(1));
    
    
    if(tag_xy.norm() != 0.0){


        Eigen::Vector6f robot_pose = quatToEuler(robot_transform);
        Eigen::Vector6f april_pose = quatToEuler(april_transform);
        Eigen::Vector2f robot_xy;
        robot_xy << robot_pose(0),robot_pose(1);
        //ROS_INFO("xRobotDist: %f",robot_xy(0));
        //ROS_INFO("yRobotDist: %f",robot_xy(1));
        
        /*
         *save the trajectories of the robot, april tag pose, and filtered tag pose
         */
        robotFile << robot_transform.stamp_ << " " << robot_pose(0) << " " << robot_pose(1) << " " << robot_pose(2) << " " << robot_pose(3) << endl;
        aprilFile << april_transform.stamp_ << " " << april_pose(0) << " " << april_pose(1) << " " << april_pose(2) << " " << april_pose(3) << endl;
        tagFile << robot_transform.stamp_ << " " << X(0) << " " << X(1) << " " << X(2) << " " << X(3) << endl;

        

        Eigen::Vector2f robot_to_tag = tag_xy - robot_xy;

        float distance2D = sqrt(pow(robot_to_tag(0),2)+pow(robot_to_tag(1),2));
        //ROS_INFO("distance: %f",distance2D);
        
        
        float angle = (cos(robot_pose(3))*robot_to_tag(1)- sin(robot_pose(3))*robot_to_tag(0))/robot_to_tag.norm();
        //ROS_INFO("angle: %f",angle);
       
        if(distance2D > goal_buffer){

            Eigen::Matrix<float, 2, 2> error;
            error << distance2D, 0.0, 0.0, angle;
            Eigen::Matrix<float, 2, 1> signal = control.update(error);
            publish(signal(1), signal(0));
        }
        else{
            ROS_INFO("HERE");
            publish(0.0,0.0);
            halt(0);
        }
    }

}

void KalmanFilter::kalmanUpdate(Eigen::Vector6f tag_pose){
    //got tag do predict then correct
    current_time = ros::Time::now();
    if(last_update.toSec() != NULL){
        predictUpdate();
        gainUpdate();
        estUpdate(tag_pose);
    }
    last_update = current_time;
}
void KalmanFilter::kalmanUpdate(){
    //no tag just predict
    current_time = ros::Time::now();
    if(last_update.toSec() != NULL){
        predictUpdate();
    }
    last_update = current_time;
    x = xHat;
    p = pHat;
}
void KalmanFilter::stateTransitionUpdate(const float dt){
    Eigen::Matrix<float, 6, 1> a;
    a.fill(dt);
    Eigen::Matrix<float, 3, 1> b;
    b.fill(0.5*pow(dt,2));
    Eigen::Matrix<float, 9, 9> A;
    A.setIdentity();
    A.diagonal(3) = a;
    A.diagonal(6) = b;

    F.setZero();
    F.topLeftCorner<9,9>() = A;
    F.bottomRightCorner<9,9>() = A;

}

void KalmanFilter::estUpdate(const Eigen::Vector6f Z){
    x = xHat + K*(Z-h*xHat);
    Eigen::Matrix18f I;
    I.setIdentity();
    p = (I-K*h)*pHat;
    
}


void KalmanFilter::gainUpdate(){

    K = pHat*hT*(((h*pHat*hT)+R).inverse());
    
}

void KalmanFilter::predictUpdate(){

    stateTransitionUpdate((float)(current_time - last_update).toSec());
    Eigen::Matrix18f FT = F.transpose();
    pHat = F*p*FT + Q; 
    xHat = F*x;
    
}
void KalmanFilter::setCovariance(const double process, const double observation){

    Eigen::Vector18f a;
    a.fill(process);
    Q.diagonal() = a;
    
    Eigen::Vector6f b;
    b.fill(observation);
    R.diagonal() = b;
}

Eigen::Vector18f KalmanFilter::getState(){
    return x;
}

Eigen::Matrix<float, 2, 1> PID::update(Eigen::Matrix<float, 2, 2> e){
    current_time = ros::Time::now();
    Eigen::Matrix<float, 2, 1> control;

    if(last_update.toSec() != NULL){
        float dt = (current_time - last_update).toSec();
        control = e*Kp + eRunning*Ki + (e-ePrev)/dt*Kd;
    
        //store previous error 
        ePrev = e;                
        eRunning += e;
  
    }else{
        control.setZero();
    }
    last_update = current_time;
    return control;
}
void PID::setGains(const double lin_P, const double lin_I, const double lin_D, const double ang_P, const double ang_I, const double ang_D){
    Kp << lin_P, ang_P;
    Ki << lin_I, ang_I;
    Kd << lin_D, ang_D;
}



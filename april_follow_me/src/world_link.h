#include <ros/ros.h>
#include <iostream>
#include <fstream>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>
using namespace std;


class WorldLink{
    public:
        WorldLink();
        

    private:
        geometry_msgs::PoseStamped dropCovariance(const geometry_msgs::PoseWithCovarianceStamped&);
        void gotPose(const geometry_msgs::PoseWithCovarianceStamped&);
        void gotTagPose(const geometry_msgs::PoseStamped&);
        void gotPath(const nav_msgs::Path& msg);
        void gotTagPath(const nav_msgs::Path& msg);
        nav_msgs::Path current_path;
        nav_msgs::Path current_tag_path;
        ros::NodeHandle node;
        ros::Subscriber pose_sub;
        ros::Subscriber tag_pose_sub;
        ros::Publisher path_pub;
        ros::Subscriber path_sub;
        ros::Publisher tag_path_pub;
        ros::Subscriber tag_path_sub;

};


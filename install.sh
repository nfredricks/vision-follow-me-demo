#!/bin/bash

cp .rosinstallFULL .rosinstall

sudo apt-get update

rosinstall .

sudo apt-get install -y python-rosinstall ros-fuerte-bosch-drivers git-core ant subversion gtk-doc-tools libglib2.0-dev libusb-1.0-0-dev gv libncurses-dev openjdk-6-jdk autopoint libgl1-mesa-dev

source setup.bash

cd rosjava_core/

./gradlew install

cd ../april_tk/april_tags_node

rosmake

cd ../../april_follow_me
 
rosmake

cd ../


